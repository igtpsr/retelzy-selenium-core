package com.retelzy.assertions;

import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.retelzy.utilities.MainUtilities;

public class BaseAssertions {

	public BaseAssertions() {

	}

	/**
	 * Method to assertBooleanValues,
	 *
	 * @param extentTest
	 *            extentTest
	 * @param condition
	 *            | Boolean value to assert
	 * @param passMessage
	 *            - pass message
	 * @param failMessage
	 *            - fail message
	 */
	public void booleanTrueAssertion(ExtentTest extentTest, boolean condition, String passMessage, String failMessage) {
		SoftAssert softAssert = new SoftAssert();
		// Assert the boolean value
		softAssert.assertTrue(condition, failMessage);

		// If statement to log the results
		if (condition) {
			MainUtilities.logPassToReport(extentTest, "Passed : " + passMessage);
		} else {
			MainUtilities.logFailedToReport(extentTest, "Failed  : " + failMessage);
		}
		softAssert.assertAll();
	}

	/**
	 * This method used to assert false assertion.
	 * 
	 * @param extentTest
	 *            extentTest
	 * @param condition
	 *            condition
	 * @param passMessage
	 *            passMessage
	 * @param failMessage
	 *            failMessage
	 */
	public void booleanFalseAssertion(ExtentTest extentTest, boolean condition, String passMessage,
			String failMessage) {
		SoftAssert softAssert = new SoftAssert();
		// Assert the boolean value
		softAssert.assertFalse(condition, failMessage);

		// If statement to log the results
		if (!condition) {
			MainUtilities.logPassToReport(extentTest, "Passed : " + passMessage);
		} else {
			MainUtilities.logFailedToReport(extentTest, "Failed : " + failMessage);
		}
		softAssert.assertAll();
	}

}
