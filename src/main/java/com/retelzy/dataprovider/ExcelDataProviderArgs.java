package com.retelzy.dataprovider;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Custom annotation for Dealing with the EXCEL dataProvider.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelDataProviderArgs {

	/**
     * The name of the excel file within your project
     * that is to be used as a DataProvider.
     * @return 
     */
    String excelFile() default "";
    
    /**
     * The name of the excel worksheet within the specified excel file
     * within your project that is to be used as a DataProvider.
     * @return 
     */
    String worksheet() default "";
}
