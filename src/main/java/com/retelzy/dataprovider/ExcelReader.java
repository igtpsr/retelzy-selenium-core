package com.retelzy.dataprovider;

import java.io.InputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.retelzy.core.helper.LogHelper;

public class ExcelReader {

	private InputStream file;

	private String sheetName;

	XSSFWorkbook workbook = null;

	XSSFSheet sheet = null;

	public ExcelReader(String filePath, String sheetN) {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			file = classLoader.getResourceAsStream(filePath);
			workbook = new XSSFWorkbook(file);
		} catch (Exception e) {
			LogHelper.getLogger().info("Exception Caught " + e.getMessage());
		}
		sheetName = sheetN;
		sheet = workbook.getSheet(sheetName);
	}

	/**
	 * This method used to get current workbook.
	 * 
	 * @return workbook
	 */
	public XSSFWorkbook getWorkbook() {
		return workbook;
	}

	/**
	 * This method used to get current worksheet.
	 * 
	 * @return sheet
	 */
	public XSSFSheet getWorkbookSheet() {
		return sheet;
	}
}
