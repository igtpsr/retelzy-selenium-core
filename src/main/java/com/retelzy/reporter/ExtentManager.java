package com.retelzy.reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.retelzy.constants.ApplicationConstant;

public class ExtentManager {

	static ExtentHtmlReporter htmlReporter;
	private static ExtentReports extent;

	private ExtentManager() {
	}

	public synchronized static ExtentReports getReporter(String filePath) {
		if (extent == null) {

			htmlReporter = new ExtentHtmlReporter(filePath);
			extent = new ExtentReports();
			extent.attachReporter(htmlReporter);
			extent.setSystemInfo("Operating System", System.getProperty("os.name"));
			extent.setSystemInfo("Browser", System.getProperty("browser"));
			extent.setSystemInfo("Environment", System.getProperty("environment"));
			extent.setSystemInfo("Application URL", System.getProperty("baseUrl"));

			htmlReporter.config().setChartVisibilityOnOpen(true);
			htmlReporter.config().setDocumentTitle(ApplicationConstant.AUTOMATION_REPORT_TITLE);
			htmlReporter.config().setReportName(ApplicationConstant.AUTOMATION_REPORT_TITLE);
			htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
			htmlReporter.config().setTheme(Theme.STANDARD);
			htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");

		}
		return extent;
	}

	public static synchronized ExtentTest startTest(String testName) {
		return extent.createTest(testName);
	}
}
