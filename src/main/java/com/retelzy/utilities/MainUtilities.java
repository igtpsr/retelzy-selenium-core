package com.retelzy.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.text.RandomStringGenerator;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.retelzy.core.helper.LogHelper;

/**
 * This class used provide string, date utility methods.
 * @author vsudrik
 *
 */
public class MainUtilities {

	/**
	 * Method to generate random string of given length
	 *
	 * @param length
	 *            - length of string you want to generate
	 * @return generated random string
	 */
	public static String generateRandomString(final int length) {
		RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a', 'z').build();
		return generator.generate(length);
	}
	
	/**
	 * This method is used to generate dynamic xpath.
	 * 
	 * @param xpath
	 *            xpath where replace parameters should mentioned in {0}
	 * @param argList
	 *            arguments to replace in string
	 * @return xpath
	 */
	public static String generateDynamicXpath(String xpath, final String... argList) {
		for (int i = 0; i < argList.length; i++) {
			xpath = xpath.replace("{" + i + "}", argList[i]);
		}
		return xpath;
	}
	
	/**
	 * This method is used to wait for provided milisecond.
	 * @param milisecond milisecond
	 */
	public static void waitFor(final long milisecond) {
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			LogHelper.getLogger().info("Exception Caught waitFor " + e.getMessage());
			Thread.currentThread().interrupt();
		}
	}
	
	/**
	 * Method to generate current date and time
	 *
	 *
	 *
	 * @return current date and time in given format ( Eg: dd-MM-YYYY hh:mm:ss )
	 */
	
	//static String s = "dd-MM-YYYY hh:mm:ss";
	
	public static String getDateTimeAsString(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat(str);
		return sdf.format(new Date());
	}
	
	/**
	 * This method is used to verify whether string is null
	 *
	 * @return returns true when string is available
	 */
	public static boolean checkNullOrEmpty(String paramString) {
		return (paramString != null && !paramString.equalsIgnoreCase("") && paramString.trim().length() != 0);
	}
	
	/**
	 * This method used to get random email id.
	 *
	 * @return email
	 */
	public static String generateRandomEmail() {
		return generateRandomString(5) + "@test.com";
	}
	
	/**
	 * Method to generate random decimal values upto 2 decimal places
	 *
	 * @return generated random decimal values upto 2 decimal places
	 */
	public static double generateRandomDecimal() {
		double doubleRandomNumber = Math.random() * 5;
		double roundOff = Math.round(doubleRandomNumber * 100.0) / 100.0;
		return roundOff;
	}
	
	/**
	 * This method used to get random digit of required length.
	 *
	 * @param n
	 *            n
	 * @return number
	 */
	public static long generateRandomDigits(int n) {
		int m = (int) Math.pow(10, n - Double.valueOf(1));
		return m + (long) new Random().nextInt(9 * m);
	}

	/**
	 * This method used to log info to report.
	 * 
	 * @param test
	 *            test
	 * @param message
	 *            message
	 */
	public static void logInfoToReport(ExtentTest test, String message) {
		test.log(Status.INFO, message);
	}

	/**
	 * This method used to log pass status to report.
	 * 
	 * @param test
	 *            test
	 * @param message
	 *            message
	 */
	public static void logPassToReport(ExtentTest test, String message) {
		test.log(Status.PASS, message);
	}

	/**
	 * This method used to log warning status to report.
	 * 
	 * @param test
	 *            test
	 * @param message
	 *            message
	 */
	public static void logWarningToReport(ExtentTest test, String message) {
		test.log(Status.WARNING, message);
	}

	/**
	 * This method used to log fail status to report.
	 * 
	 * @param test
	 *            test
	 * @param message
	 *            message
	 */
	public static void logFailedToReport(ExtentTest test, String message) {
		test.log(Status.FAIL, message);
	}

	/**
	 * This method is used to get SQL query which are formatted using provided
	 * parameter values.
	 *
	 * @param query
	 *            SQL query
	 * @param parameters
	 *            Parameter values
	 * @return Formatted SQL query
	 */
	public static String getFormattedQuery(final String query, final String... parameters) {
		return getFormattedString(query, parameters);
	}

	/**
	 * This method is used to get string which are formatted using provided
	 * parameter values.
	 *
	 * @param stringTobeFormated
	 *            : String that needs to be formated
	 * @param parameters
	 *            Parameter values
	 * @return Formatted SQL query
	 */
	public static String getFormattedString(final String stringTobeFormated, final String... parameters) {
		String formattedString = stringTobeFormated.trim();
		for (int i = 0; i < parameters.length; i++) {
			formattedString = formattedString.replace("{" + i + "}", String.valueOf(parameters[i]));
		}
		return formattedString;
	}
}
