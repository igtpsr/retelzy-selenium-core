package com.retelzy.constants;

/**
 * This class is used to define java script constant.
 * @author vsudrik
 *
 */
public class JavaScriptConstants {

	/**
	 * constructor.
	 */
	private JavaScriptConstants() {

	}

	/** ARGUMENTS_SCROLL_INTO_VIEW. */
	public static final String ARGUMENTS_SCROLL_INTO_VIEW = "arguments[0].scrollIntoView();";

	/** JAVASCRIPT_CLICK_ARGUMENT. */
	public static final String JAVASCRIPT_CLICK_ARGUMENT = "var el=arguments[0]; setTimeout(function() { el.click(); }, 100);";
}
