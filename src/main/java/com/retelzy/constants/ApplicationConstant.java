package com.retelzy.constants;

import org.apache.commons.lang3.StringUtils;

import com.retelzy.core.helper.ParameterHelper;

/**
 * This class is used to define application constant.
 * 
 * @author vsudrik
 *
 */
public class ApplicationConstant {

	/**
	 * constructor.
	 */
	private ApplicationConstant() {

	}

	/**
	 * AUTOMATION_REPORT.
	 */
	public static final String AUTOMATION_REPORT = StringUtils.isBlank(System.getProperty("reportName"))
			? ParameterHelper.getParameterDefaultValue("reportName") // Will read this value from property file
			: System.getProperty("reportName");

	/**
	 * AUTOMATION_SUMMARY_REPORT.
	 */
	public static final String AUTOMATION_SUMMARY_REPORT = StringUtils.isBlank(System.getProperty("summaryReportName"))
			? ParameterHelper.getParameterDefaultValue("summaryReportName") // Will read this value from property file
			: System.getProperty("summaryReportName");
	
	/**
	 * AUTOMATION_REPORT_TITLE.
	 */
	public static final String AUTOMATION_REPORT_TITLE = StringUtils.isBlank(System.getProperty("reportTitle"))
			? ParameterHelper.getParameterDefaultValue("reportTitle")// Will read this value from property file
			: System.getProperty("reportTitle");

}
