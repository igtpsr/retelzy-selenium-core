package com.retelzy.constants;

public class Constants {

	/**
	 * constructor.
	 */
	private Constants() {

	}

	/**
	 * BROWSER_NAME_PARAMETER.
	 */
	public static final String BROWSER_NAME_PARAMETER = "browser";

	/**
	 * SELENIUM_SERVER_PARAMETER.
	 */
	public static final String SELENIUM_SERVER_PARAMETER = "gridHub";

	/**
	 * CHROME.
	 */
	public static final String CHROME = "chrome";
	
	/**
	 * FIREFOX.
	 */
	public static final String FIREFOX = "firefox";

	
	/**
	 * IE.
	 */
	public static final String IE = "ie";


	/**
	 * DEFAULT_SELENIUM_SERVER.
	 */
	public static final String DEFAULT_SELENIUM_SERVER = "http://localhost:4444";
	
	/**
	 * USER_DIR.
	 */
	public static final String USER_DIR = "user.dir";
	
	/**
	 * WEB_DRIVER_CHROME.
	 */
	public static final String WEB_DRIVER_CHROME = "webdriver.chrome.driver";
	
	/**
	 * WEB_DRIVER_FIREFOX.
	 */
	public static final String WEB_DRIVER_FIREFOX = "webdriver.gecko.driver";
	
	/**
	 * WEB_DRIVER_IE.
	 */
	public static final String WEB_DRIVER_IE = "webdriver.ie.driver";
	
	/**
	 * CHROME_DRIVER.
	 */
	public static final String CHROME_DRIVER = "chromedriver.exe";
	
	/**
	 * FIREFOX_DRIVER.
	 */
	public static final String FIREFOX_DRIVER = "geckodriver.exe";
	
	/**
	 * IE_DRIVER.
	 */
	public static final String IE_DRIVER = "IEDriverServer.exe";
}
