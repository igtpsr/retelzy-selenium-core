package com.retelzy.core.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to log the event.
 * 
 * @author vsudrik
 *
 */
public class LogHelper {

	/**
	 * constructor.
	 */
	private LogHelper() {

	}

	/**
	 * This method is used to return logger object.
	 * 
	 * @return logger object
	 */
	public static Logger getLogger() {

		final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		/*
		 * stackTrace[0] is for Thread.currentThread().getStackTrace() stackTrace[1] is
		 * for this method log()
		 */
		String className = stackTrace[2].getClassName();
		return LoggerFactory.getLogger(className);
	}
}
