package com.retelzy.core.helper;

import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;

public class ParameterHelper {

	/**
	 * constructor.
	 */
	private ParameterHelper() {

	}

	private static final Logger log = LogHelper.getLogger();

	private static Properties properties;

	static {
		try {
			loadParameters();
		} catch (Exception e) {
			ExceptionHelper.rethrow(e);
		}
	}

	private static void loadParameters() {
		properties = new Properties();
		String filePath = "application.properties";
		try (InputStream inputStream = ParameterHelper.class.getClassLoader().getResourceAsStream(filePath)) {
			properties.load(inputStream);
		} catch (Exception ex) {
			log.error("Fail to load properties from {}", filePath, ex);
		}
		properties.putAll(System.getenv());
		properties.putAll(System.getProperties());
	}

	public static String getParameterDefaultValue(String key) {
		return properties.getProperty(key);
	}

	public static Set<String> getParameterNames() {
		return properties.stringPropertyNames();
	}
}
