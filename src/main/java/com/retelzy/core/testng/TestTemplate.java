package com.retelzy.core.testng;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.util.Strings;

import com.retelzy.constants.Constants;
import com.retelzy.core.helper.ExceptionHelper;
import com.retelzy.core.helper.LogHelper;
import com.retelzy.core.helper.ParameterHelper;
import com.retelzy.core.selenium.PageTemplate;
public abstract class TestTemplate {

	protected static final Logger log = LogHelper.getLogger();

	protected WebDriver driver;

	@BeforeMethod(alwaysRun = true)
	public void beforeTest(ITestContext context) {

		try {

			log.info("Initializing parameters.");

			Map<String, String> parameters = context.getCurrentXmlTest().getAllParameters();

			ParameterHelper.getParameterNames().forEach(key -> {
				String value = parameters.get(key);
				if (Strings.isNullOrEmpty(value)) {
					value = ParameterHelper.getParameterDefaultValue(key);
					parameters.put(key, value);
				}
			});

			String key;
			String value;

			key = Constants.BROWSER_NAME_PARAMETER;
			value = parameters.get(key);
			if (Strings.isNullOrEmpty(value)) {
				value = Constants.CHROME;
				parameters.put(key, value);
			}
			key = Constants.SELENIUM_SERVER_PARAMETER;
			value = parameters.get(key);
			if (Strings.isNullOrEmpty(value)) {
				value = Constants.DEFAULT_SELENIUM_SERVER;
				parameters.put(key, value);
			}

			String name = parameters.get(Constants.BROWSER_NAME_PARAMETER);
			final String seleniumServer;
			if (StringUtils.isBlank(System.getProperty(Constants.SELENIUM_SERVER_PARAMETER))) {
				seleniumServer = ParameterHelper.getParameterDefaultValue(Constants.SELENIUM_SERVER_PARAMETER);
			} else {
				seleniumServer = System.getProperty(Constants.SELENIUM_SERVER_PARAMETER);
			}
			log.info("Initializing WebDriver {} {}", name, seleniumServer);
			driver = new TestHelper().getDriver(name, seleniumServer);
			log.info("Initializing Pages.");
			initPages();

		} catch (Exception e) {
			ExceptionHelper.rethrow(e);
		}
	}

	private void initPages() throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		List<PageTemplate> pages = new ArrayList<>();
		Field[] fields = getClass().getDeclaredFields();
		for (Field field : fields) {
			Class<?> fieldType = field.getType();
			if (PageTemplate.class.isAssignableFrom(fieldType)) {
				field.setAccessible(true);
				PageTemplate page = (PageTemplate) field.get(this);
				if (page == null) {
					page = (PageTemplate) fieldType.newInstance();
					field.set(this, page);
				}
				field.setAccessible(false);
				log.info("{}", page);
				pages.add(page);
			}
		}
		pages.forEach(page -> page.init(driver));
	}
}
