package com.retelzy.core.testng;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;

import com.retelzy.constants.Constants;
import com.retelzy.core.helper.LogHelper;
import com.retelzy.core.helper.ParameterHelper;

public class TestHelper {

	private static final Logger log = LogHelper.getLogger();
	WebDriver driver;

	public WebDriver getDriver(String browserName, String seleniumServer) {
		log.info("Initializing WebDriver...");
		try {
			final String enabledGrid;
			if (StringUtils.isBlank(System.getProperty("enableGrid"))) {
				enabledGrid = ParameterHelper.getParameterDefaultValue("enableGrid");
			} else {
				enabledGrid = System.getProperty("enableGrid");
			}
			log.info("Set Enable Grid to ..." + enabledGrid);
			if (Boolean.parseBoolean(enabledGrid)) {
				log.info("Initializing Remote WebDriver...");
				driver = getRemoteDriver(browserName, seleniumServer);
			} else {
				if (browserName.equals(Constants.CHROME)) {
					System.setProperty(Constants.WEB_DRIVER_CHROME, System.getProperty(Constants.USER_DIR)
							+ File.separator + "drivers" + File.separator + Constants.CHROME_DRIVER);
					driver = new ChromeDriver(getChromeOptions());
				} else if (browserName.equals(Constants.IE)) {
					final InternetExplorerOptions internetExplorerOptions = getIEOptions();
					internetExplorerOptions.introduceFlakinessByIgnoringSecurityDomains();
					internetExplorerOptions.requireWindowFocus();
					internetExplorerOptions.destructivelyEnsureCleanSession();
					internetExplorerOptions.ignoreZoomSettings();
					System.setProperty(Constants.WEB_DRIVER_IE, System.getProperty(Constants.USER_DIR)
							+ File.separator + "drivers" + File.separator + Constants.IE_DRIVER);
					driver = new InternetExplorerDriver(internetExplorerOptions);
				} else if (browserName.equals(Constants.FIREFOX)) {
					System.setProperty(Constants.WEB_DRIVER_FIREFOX, System.getProperty(Constants.USER_DIR)
							+ File.separator + "drivers" + File.separator + Constants.FIREFOX_DRIVER);
					driver = new FirefoxDriver(getFirefoxDesiredCapabilities());
				}
			}
		} catch (Throwable e) {
		}
		return driver;
	}

	/**
	 * This method is used to get remote driver object used for parallel execution
	 * with grid.
	 *
	 * @param browserName
	 *            browserName
	 * @param remoteAddress
	 *            remoteAddress
	 * @return Remote Web Driver object
	 * @throws MalformedURLException
	 *             Exception
	 */
	private RemoteWebDriver getRemoteDriver(final String browserName, final String remoteAddress)
			throws MalformedURLException {
		RemoteWebDriver driver = null;
		if (browserName.equals(Constants.FIREFOX)) {
			driver = new RemoteWebDriver(new URL(remoteAddress), getFirefoxDesiredCapabilities());
		} else if (browserName.equals(Constants.CHROME)) {
			driver = new RemoteWebDriver(new URL(remoteAddress), getChromeOptions());
		} else if (browserName.equals(Constants.IE)) {
			driver = new RemoteWebDriver(new URL(remoteAddress), getIEOptions());
		}
		if (driver != null) {
			driver.setFileDetector(new LocalFileDetector());
		}
		return driver;
	}

	/**
	 * This method is used to get Chrome options with desired capabilities.
	 *
	 * @return chrome option
	 */
	private ChromeOptions getChromeOptions() {
		final ChromeOptions options = new ChromeOptions();
		final Map<String, Object> prefs = new ConcurrentHashMap<>();
		prefs.put("download.default_directory", System.getProperty(Constants.USER_DIR) + File.separator + "downloads");
		prefs.put("safebrowsing.enabled", "true");
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-xss-auditor");
		options.addArguments("--disable-infobars");
		options.setExperimentalOption("excludeSwitches", Arrays.asList("ignore-certificate-errors"));
		options.setExperimentalOption("prefs", prefs);
		final DesiredCapabilities sCaps = DesiredCapabilities.chrome();
		sCaps.setAcceptInsecureCerts(true);
		sCaps.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(sCaps);
		return options;
	}

	/**
	 * This method is used to get Internet Explorer options with desired
	 * capabilities.
	 *
	 * @return ie options
	 */
	private InternetExplorerOptions getIEOptions() {
		final InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
		internetExplorerOptions.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
				true);
		internetExplorerOptions.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		internetExplorerOptions.setCapability("ignoreProtectedModeSettings", true);
		internetExplorerOptions.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
		internetExplorerOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		final DesiredCapabilities sCaps = DesiredCapabilities.internetExplorer();
		sCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		internetExplorerOptions.merge(sCaps);
		return internetExplorerOptions;
	}

	/**
	 * This method is used to get Firefox options with desired capabilities.
	 *
	 * @return firefoxOptions
	 */

	private FirefoxOptions getFirefoxDesiredCapabilities() {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.dir",
				System.getProperty(Constants.USER_DIR) + File.separator + "downloads");
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("pdfjs.disabled", true);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-excel,content/unknown");
		final FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.setProfile(profile);
		return firefoxOptions;
	}
}
