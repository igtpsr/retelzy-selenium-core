package com.retelzy.core.selenium;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;

import com.retelzy.constants.JavaScriptConstants;
import com.retelzy.core.helper.LogHelper;
import com.retelzy.core.helper.ParameterHelper;

/**
 * This class is used to provided wrapper methods over the selenium inbuilt
 * method.
 * 
 * @author vsudrik
 *
 */
public class PageTemplate {

	protected static final Logger log = LogHelper.getLogger();
	protected WebDriver driver;

	/**
	 * This method used to initialize the driver.
	 * 
	 * @param driver
	 *            driver
	 */
	public void init(WebDriver driver) {
		log.info("Initializing Page: {}", this.getClass().getName());
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		initElements(driver);
	}

	/**
	 * This method used to initialize the page elements.
	 * 
	 * @param driver
	 *            driver
	 */
	public void initElements(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	/**
	 * This method is used to scroll to given web element.
	 *
	 * @param element
	 *            - Web element to scroll at.
	 */
	public void scrollOnElement(final WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		js.executeScript(JavaScriptConstants.ARGUMENTS_SCROLL_INTO_VIEW, element);
	}

	/**
	 * This method is used to click given web element.
	 *
	 * @param webObject
	 *            Web element to be clicked
	 */
	public void clickOnObjectWithJavaScript(final WebElement webObject) {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		js.executeScript(JavaScriptConstants.JAVASCRIPT_CLICK_ARGUMENT, webObject);
	}

	/**
	 * This method is used to drag an element to targeted element.
	 *
	 * @param sourceElement
	 *            element to be dragged.
	 * @param targetElement
	 *            element to which dragged element will be released.
	 */
	public void dragAndDrop(final WebElement sourceElement, final WebElement targetElement) {
		final Actions builder = new Actions(this.driver);
		builder.dragAndDrop(sourceElement, targetElement).perform();
	}

	/**
	 * This method is used to get active element.
	 *
	 * @return active element
	 */
	public WebElement getActiveElement() {
		return this.driver.switchTo().activeElement();
	}

	/**
	 * This method is used to press given key.
	 *
	 * @param key
	 *            key to be pressed
	 */
	public void keyDown(final Keys key) {
		final Actions actions = new Actions(this.driver);
		actions.keyDown(key).build().perform();
	}

	/**
	 * This method is used to release given pressed key.
	 *
	 * @param key
	 *            key to be released
	 */
	public void keyUp(final Keys key) {
		final Actions actions = new Actions(this.driver);
		actions.keyUp(key).build().perform();
	}

	/**
	 * This method used to wait for element to be visible.
	 * 
	 * @param by
	 *            identifier
	 * @return object
	 */
	public Object waitForElementToVisible(final By by) {
		Object object = null;
		try {
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(
							Duration.ofSeconds(Integer.valueOf(ParameterHelper.getParameterDefaultValue("timeout"))))
					.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
			object = wait.until(new ExpectedCondition<Object>() {
				@Override
				public Object apply(WebDriver arg0) {
					return driver.findElement(by);
				}
			});
		} catch (TimeoutException e) {
			LogHelper.getLogger().info("Element not visible within given time..");
		}
		return object;
	}

	/**
	 * This method used to perform context click on given element.
	 * 
	 * @param element
	 *            element
	 */
	public void contextClickOnElement(final WebElement element) {
		final Actions actions = new Actions(this.driver);
		actions.contextClick(element).build().perform();
	}

	/**
	 * This method used to enter text into given field.
	 * 
	 * @param element
	 *            element
	 */
	public void setText(final WebElement element, final String input) {
		element.clear();
		element.sendKeys(input);
	}

	/**
	 * This method used to click on given element.
	 * 
	 * @param element
	 *            element
	 */
	public void clickOnElement(final WebElement element) {
		element.click();
	}

	/**
	 * this method used to get text from given element.
	 * 
	 * @param element
	 *            element
	 * @return text
	 */
	public String getText(final WebElement element) {
		return element.getText();
	}

	/**
	 * This method is used to verify the given element is displayed or not.
	 * 
	 * @param element
	 *            element
	 * @return true if displayed else false
	 */
	public boolean isElementDisplayed(final WebElement element) {
		return element.isDisplayed();
	}

	/**
	 * this method used to launch an given application.
	 * 
	 * @param appURL
	 *            appURL
	 */
	public void launchApp(final String appURL) {
		driver.get(appURL);
	}

	/**
	 * This method used to move cursor to an given element.
	 * 
	 * @param element
	 *            element
	 */
	public void moveToElement(final WebElement element) {
		final Actions actions = new Actions(this.driver);
		actions.moveToElement(element).perform();
	}

	/**
	 * This method used to move cursor to an given element and click.
	 * 
	 * @param element
	 *            element
	 */
	public void moveToElementAndClick(final WebElement element) {
		final Actions actions = new Actions(this.driver);
		actions.moveToElement(element).click().build().perform();
	}

	/**
	 * This method used to enter text into given field.
	 * 
	 * @param element
	 *            element
	 */
	public void setKey(final WebElement element, final String input) {
		element.sendKeys(input);
	}

	/**
	 * This method used to get value for given element attribute.
	 * 
	 * @param element
	 *            element
	 * @param attribute
	 *            attribute
	 * @return text
	 */
	public String getTextByAttribute(final WebElement element, final String attribute) {
		return element.getAttribute(attribute);
	}

	/**
	 * This method is used to refresh page.
	 */
	public void refreshPage() {
		this.driver.navigate().refresh();
	}

	/**
	 * This method used to scroll down.
	 */
	public void scrollDown() {
		final Actions actions = new Actions(this.driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
	}
}
