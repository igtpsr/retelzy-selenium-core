package com.retelzy.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.retelzy.core.helper.LogHelper;

public class QueryManager {

	/**
	 * QueryManager object - INSTANCE.
	 */
	private static final QueryManager INSTANCE = new QueryManager();

	/**
	 * Statement object - statement.
	 */
	private Statement statement;

	/**
	 * ResultSet object - resultSet.
	 */
	private ResultSet resultSet;

	/**
	 * Constructor - QueryManager.
	 */
	private QueryManager() {
	}

	/**
	 * This method gets the instance of QueryManager class.
	 *
	 * @return instance of QueryManager class
	 */
	public static QueryManager getInstance() {
		return INSTANCE;
	}

	/**
	 * This method creates a database connection statement for given SQL query.
	 *
	 * @return statement
	 */
	public Statement createStatement() {
		final Connection connection = ConnectionManager.getInstance().openConnection();
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			LogHelper.getLogger().info("Exception occured while creating a database connection statement." + e);
		}
		return statement;
	}

	/**
	 * This method used to get given column values as a list.
	 *
	 * @param query
	 *            query
	 * @param columnName
	 *            columnName
	 * @return columnValues
	 */
	public List<String> getColumnValueAsList(final String query, final String columnName) {
		List<String> columnValues = new ArrayList<String>();
		try {
			ResultSet resultSet = createStatement().executeQuery(query);
			try {
				while (resultSet.next()) {
					columnValues.add(resultSet.getString(columnName));
				}
			} finally {
				try {
					resultSet.close();
					statement.close();
				} catch (SQLException e) {
					LogHelper.getLogger().info("Exception occured while closing result set and statement." + e);
				}
			}
		} catch (SQLException e) {
			LogHelper.getLogger().info("Exception occured while iterating result set." + e);
		}
		return columnValues;
	}

	/**
	 * This method used to get given column value.
	 *
	 * @param query
	 *            query
	 * @param columnName
	 *            columnName
	 * @return columnValue
	 */
	public String getColumnValue(final String query, final String columnName) {
		String columnValue = null;
		try {
			ResultSet resultSet = createStatement().executeQuery(query);
			resultSet.next();
			columnValue = resultSet.getString(columnName);
		} catch (SQLException e) {
			LogHelper.getLogger().info("Exception occured while iterating result set." + e);
		}
		return columnValue;
	}

	/**
	 * This method used to update the result.
	 *
	 * @param query
	 *            query
	 */
	public void updateResult(final String query) {
		try {
			createStatement().executeUpdate(query);
		} catch (SQLException e) {
			LogHelper.getLogger().info("Exeception occured while executing update query." + e);
		}
	}
}
