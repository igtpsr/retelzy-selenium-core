package com.retelzy.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

import com.retelzy.core.helper.LogHelper;
import com.retelzy.core.helper.ParameterHelper;

public class ConnectionManager {

	/** ConnectionManager object - INSTANCE. */
	private static final ConnectionManager INSTANCE = new ConnectionManager();

	/** Connection object. */
	private Connection connection = null;

	/** Constructor - ConnectionManager(). */
	private ConnectionManager() {
	}

	/**
	 * This method returns the instance of ConnectionManager.
	 *
	 * @return instance of ConnectionManager
	 */
	public static ConnectionManager getInstance() {
		return INSTANCE;
	}

	/**
	 * This method allows to open a database connection.
	 *
	 * @return Returns connection object
	 */

	public Connection openConnection() {
		try {
			if (connection == null || connection.isClosed()) {
				connection = getConnection();
			}
		} catch (SQLException e) {
			LogHelper.getLogger().info("Unable to open database connection." + e);
		}
		return connection;
	}

	/**
	 * This method returns the database Connection.
	 *
	 * @return Returns connection object
	 */
	private Connection getConnection() {
		String dbUrl = StringUtils.isBlank(System.getProperty("dbUrl"))
				? ParameterHelper.getParameterDefaultValue("dbUrl")
				: System.getProperty("dbUrl");
		String dbUserName = StringUtils.isBlank(System.getProperty("dbUserName"))
				? ParameterHelper.getParameterDefaultValue("dbUserName")
				: System.getProperty("dbUserName");
		String dbPassword = StringUtils.isBlank(System.getProperty("dbPassword"))
				? ParameterHelper.getParameterDefaultValue("dbPassword")
				: System.getProperty("dbPassword");
		String dbDriver = StringUtils.isBlank(System.getProperty("dbDriver"))
						? ParameterHelper.getParameterDefaultValue("dbDriver")
						: System.getProperty("dbDriver");
		try {
			Class.forName(dbDriver);
			connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		} catch (SQLException | ClassNotFoundException e) {
			LogHelper.getLogger().info("Unable to create database connection." + e);
		}
		return connection;
	}

	/**
	 * This method is used to close the database Connection.
	 */
	public void closeConnection() {
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException e2) {
			LogHelper.getLogger().info("Unable to close database connection." + e2);
		}
	}
}
